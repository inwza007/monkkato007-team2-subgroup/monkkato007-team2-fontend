import type { Material } from './Material'
type StockItem = {
  id: number
  name: string
  unit: number
  price: number
  stockId: number
  material: Material
}

export { type StockItem }
