import { ref } from 'vue'

type Gender = 'male' | 'female' | 'others'
type roles = 'admin' | 'user'
type User = {
  id: number
  email: string
  password: string
  fullName: string
  gender: Gender
  roles: roles
  timein: string
  timeout: string
  timework: number
  status: string
}

export const users: User[] = [
  {
    id: 1,
    email: 'user1@gmail.com',
    password: 'Pass@Word123',
    fullName: 'Chanon Thansawad',
    gender: 'male',
    roles: 'admin',
    timein: '',
    timeout: '',
    timework: 24,
    status: ''
  }
  // ,
  // {
  //   id: 2,
  //   email: 'user2@gmail.com',
  //   password: 'Pass@Word123',
  //   fullName: 'Teekayu Kongtara',
  //   gender: 'male',
  //   roles: ['user'],
  //   timein: '',
  //   timeout: '',
  //   timework: 0,
  //   status:''
  // },
  // {
  //   id: 3,
  //   email: 'user3@gmail.com',
  //   password: 'Pass@Word123',
  //   fullName: 'Suppakij Sripong',
  //   gender: 'male',
  //   roles: ['user'],
  //   timein: '',
  //   timeout: '',
  //   timework: 0,
  //   status:''
  // },
  // {
  //   id: 4,
  //   email: 'User4@gmail.com',
  //   password: 'Pass@Word123',
  //   fullName: 'Phattharaphon Jansanga',
  //   gender: 'male',
  //   roles: ['user'],
  //   timein: '',
  //   timeout: '',
  //   timework: 0,
  //   status:''
  // },
  // {
  //   id: 5,
  //   email: 'user5@gmail.com',
  //   password: 'Pass@Word123',
  //   fullName: 'Pakawat authored',
  //   gender: 'male',
  //   roles: ['user'],
  //   timein: '',
  //   timeout: '',
  //   timework: 0,
  //   status:''
  // },
  // {
  //   id: 6,
  //   email: 'user6@gmail.com',
  //   password: 'Pass@Word123',
  //   fullName: 'Yanagron Yingyong ',
  //   gender: 'male',
  //   roles: ['user'],
  //   timein: '',
  //   timeout: '',
  //   timework: 0,
  //   status:''
  // },
  // {
  //   id: 7,
  //   email: 'user7@gmail.com',
  //   password: 'Pass@Word123',
  //   fullName: 'Jirapat Thitiworn',
  //   gender: 'male',
  //   roles: ['user'],
  //   timein: '',
  //   timeout: '',
  //   timework: 0,
  //   status:''
  // }
]
export const loginData = ref<User[]>([])
export { users as user }
export type { User, Gender, roles }
