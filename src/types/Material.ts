type Material = {
  material_id: number
  material_name: string
  material_qty: number
  material_status: string
  material_min: number
  material_max: number
  material_price: number
  material_unit: string
  material_Im: 0
}

export { type Material }
