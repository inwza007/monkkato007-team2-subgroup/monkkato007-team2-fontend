import type { Product } from './Product'

type ReceiptItem = {
  id?: number
  name: string
  price: number
  qty: number
  productId: number
  product?: Product
}

export { type ReceiptItem }
