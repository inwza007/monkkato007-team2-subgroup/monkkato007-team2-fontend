type Attendance = {
  id: number
  email: string
  fullName: string
  timeIn: Date | null
  timeOut: Date | null
}

export { Attendance }
