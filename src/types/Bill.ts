type Type = 'Electricity' | 'Water' | 'Rent' | 'Other'
type Bill = {
  id?: number
  userId: string
  branchId: string
  // createdDateString?: String
  billType?: Type
  image?: string
  detail: string
  amount: number
  updated?: string
}

export type { Bill }
