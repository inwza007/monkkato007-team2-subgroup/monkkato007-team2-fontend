import type { Type } from './Type'
type subType = 'Hot' | 'Cool' | 'Frappe' | '-'
type size = 'S' | 'M' | 'L' | '-'
type sweetLevel = '0%' | '25%' | '50%' | '75%' | '100%' | '-'

type Product = {
  id?: number
  name: string
  price: number
  type: Type
  subType: subType[]
  size: size[]
  sweetLevel: sweetLevel[]
  image: string
}
function getImageUrl(product: Product) {
  return `http://localhost:3000/images/products/${product.image}`
  // return `http://localhost:3000/images/products/noimage.jpg`
}
export { type Product, getImageUrl }
