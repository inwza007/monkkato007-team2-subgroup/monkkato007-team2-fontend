import type { Branch } from "./Branch";
import type { Material } from "./Material";

type Stock = {
  stock_id: number;
  stock_qty: number;
  material_status: string;
  materialId: number;
  branchId: number;
  material: Material;
  branch: Branch;
}

export { type Stock }
