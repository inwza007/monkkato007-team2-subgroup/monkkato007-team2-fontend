type Listnamepay = {
  // id: number

  // name: string
  // bank:string
  // numberbank: string
  // salary:number
  id?: number;
  name: string;
  status: string
  salary: number;
  workhours: number;
  roles: string
  date: Date;
}

export { type Listnamepay }
