type status = 'Active' | 'Inactive'
type Promotion = {
  id: number
  name: string
  startdate: Date | null
  enddate: Date | null
  status: status
  discount: number
}

export type { Promotion }
