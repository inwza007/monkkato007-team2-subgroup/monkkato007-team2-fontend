type Member = {
  id?: number
  fullName: string
  tel: string
  email: string
  password: string
  point: string
  birthday: string
  usedPoint: string
}

export type { Member }
