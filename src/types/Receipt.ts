import type { Branch } from './Branch'
import type { Member } from './Member'
import type { ReceiptItem } from './ReceiptItem'
import type { User } from './User'

type Receipt = {
  id?: number
  createdDate: Date
  total: number
  discount: number
  net: number
  amount: number
  change: number
  paymentType: string
  userId: number
  user?: User
  memberId: number
  members?: Member
  branchId: number
  branchs?: Branch
  updated?: string
  receiptDetails?: ReceiptItem[]
}

export type { Receipt }
