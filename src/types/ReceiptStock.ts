import type { StockItem } from '@/types/StockItem'
type ReceiptStock = {
  id: number
  createdDate?: Date
  createdDateString: string
  total: number
  change: number
  paymentType: string
  receiptStockItem: StockItem[]
}

export type { ReceiptStock }
