import http from './http'
import type { Bill } from '@/types/Bill'

function addBill(bill: Bill) {
  return http.post('/bill', bill)
}

function updateBill(bill: Bill) {
  return http.patch(`/bill/${bill.id}`, bill)
}

function delBill(bill: Bill) {
  return http.delete(`/bill/${bill.id}`)
}

function getBill(id: number) {
  return http.get(`/bill/${id}`)
}

function getBills() {
  return http.get('/bill')
}

export default { addBill, updateBill, delBill, getBill, getBills }
