import type { Receipt } from '@/types/Receipt'
import type { ReceiptItem } from '@/types/ReceiptItem'
import http from './http'

type ReceiptDto = {
  receiptDetails: {
    productId: number
    qty: number
  }[]
  payType: string
  amount: number
  userId: number
  memberId: number
}

function addOrder(receipt: Receipt, receiptItems: ReceiptItem[]) {
  const receiptDto: ReceiptDto = {
    receiptDetails: [],
    userId: 0,
    payType: receipt.paymentType,
    amount: receipt.amount,
    memberId: receipt.memberId
  }
  receiptDto.userId = receipt.userId
  console.log('service user ' + receiptDto.userId)
  receiptDto.receiptDetails = receiptItems.map((item) => {
    return {
      productId: item.productId,
      qty: item.qty
    }
  })
  console.log('service receiptdetail ' + JSON.stringify(receiptDto.receiptDetails))
  return http.post('/receipt', receiptDto)
}

function getReceitp(id: number) {
  return http.get(`/receipt/${id}`)
}

function getReceitps() {
  return http.get('/receipt')
}

export default { addOrder, getReceitp, getReceitps }
