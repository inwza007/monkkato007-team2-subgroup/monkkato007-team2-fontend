import http from './http'
import type { Stock } from '@/types/stock'

function addStock(stock: Stock) {
  const formData = new FormData()
  formData.append('stock_qty', stock.stock_qty.toString())
  formData.append('stock_status', stock.material_status)
  formData.append('stockId', stock.materialId.toString())
  formData.append('branchId', stock.branchId.toString())
  return http.post('/stock', stock)
}

function updateStock(stock: Stock) {
  return http.patch(`/stock/${stock.stock_id}`, stock)
}

function delStock(stock: Stock) {
  return http.delete(`/stock/${stock.stock_id}`)
}

function getStock(id: number) {
  //fix
  return http.get(`/stock/${id}`)
}

function getStocks() {
  return http.get('/stock')
}

function getLowStocks() {
  return http.get('/stock/low')
}

function getOutStocks() {
  return http.get('/stock/out')
}

function getFullStocks() {
  return http.get('/stock/full')
}

export default { addStock, updateStock, delStock, getStock, getStocks, getLowStocks, getOutStocks, getFullStocks }
