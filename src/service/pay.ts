import type { Listnamepay } from '@/types/Listnamepay'
import http from './http'

function addPay(pay: Listnamepay) {
  const params = new URLSearchParams()

  params.append('approve', pay.status)
  params.append('date', pay.date.toISOString())
  params.append('total_salary', pay.salary.toString())
  if (pay.id !== undefined) {
    params.append('userid', pay.id.toString())
  }
  
  return http.post('/pay', params)
}

function updatePay(pay: Listnamepay) {
  const params = new URLSearchParams()
  params.append('approve', pay.status)
  params.append('date', pay.date.toISOString())
  params.append('total_salary', pay.salary.toString())
  if (pay.id !== undefined) {
    params.append('userid', pay.id.toString())
  }
  return http.patch(`/pay/${pay.id}`, pay)
}

function delPay(pay: Listnamepay) {
  return http.delete(`/pay/${pay.id}`)
}

function getPay(id: number) {
  return http.get(`/pay/${id}`)
}

function getAttendance(id: number) {
  return http.get(`/attendance/${id}`)
}

function getPays() {
  return http.get('/pay')
}

function getPaysByUserName(userId: number) {
  return http.get('/pay/userid/' + userId)
}

export default { addPay, updatePay, delPay, getPay, getPays,getPaysByUserName,getAttendance }
