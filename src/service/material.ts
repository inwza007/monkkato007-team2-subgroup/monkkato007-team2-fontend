import type { Material } from '@/types/Material'
import http from './http'

function addMaterial(material: Material) {
  const formData = new FormData()
  formData.append('material_name', material.material_name)
  formData.append('material_min', material.material_min.toString())
  formData.append('material_qty', material.material_qty.toString())
  formData.append('material_status', material.material_status)
  formData.append('material_unit', material.material_unit)
  formData.append('material_price', material.material_price.toString())
  return http.post('/Material', material)
}

function updateMaterial(material: Material) {
  return http.patch(`/Material/${material.material_id}`, material)
}

function delMaterial(material: Material) {
  return http.delete(`/Material/${material.material_id}`)
}

function getMaterial(id: number) {
  //fix
  return http.get(`/Material/${id}`)
}

function getMaterials() {
  return http.get('/Material')
}

export default { addMaterial, updateMaterial, delMaterial, getMaterial, getMaterials }
