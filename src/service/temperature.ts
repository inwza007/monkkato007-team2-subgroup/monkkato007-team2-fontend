import http from './http'

type ReturnData = {
  celsius: number
  fahrenhiet: number
}

async function convert(celsius: number): Promise<number> {
  console.log('Service: call Convert')
  console.log(`/temperature/convert/${celsius}`)
  const res = await http.post(`/temperature/convert`, {
    celsius: celsius
  })
  const convertResult = res.data as ReturnData
  console.log('Service: Finish call Convert')
  return convertResult.fahrenhiet
}

export default { convert }
