import http from './http'
import type { Attendance } from '@/types/Attendance'

function addAttendance(attendance: Attendance) {
  return http.post('/attendance', attendance)
}

function updateAttendance(attendance: Attendance) {
  return http.patch(`/attendance/${attendance.id}`, attendance)
}

function delAttendance(attendance: Attendance) {
  return http.delete(`/attendance/${attendance.id}`)
}

function getAttendances() {
  return http.get('/attendance')
}
function getAttendance(id: number) {
  return http.get(`/attendance/${id}`)
}

export default {
  addAttendance,
  updateAttendance,
  delAttendance,
  getAttendances,
  getAttendance
}
