import type { Member } from '@/types/Member'
import http from './http'

function addMember(member: Member) {
  return http.post('/member', member)
}

function updateMember(member: Member) {
  return http.patch(`/member/${member.id}`, member)
}

function delMember(member: Member) {
  return http.delete(`/member/${member.id}`)
}

function getMembers() {
  return http.get('/member')
}
function getMember(id: number) {
  return http.get(`/member/${id}`)
}

export default {
  addMember,
  updateMember,
  delMember,
  getMembers,
  getMember
}
