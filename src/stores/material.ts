import { nextTick, ref } from 'vue'
import { defineStore } from 'pinia'
import type { Material } from '@/types/Material'
import materialService from '@/service/material'
import type { ReceiptStock } from '@/types/ReceiptStock'
import type { StockItem } from '@/types/StockItem'

export const useMaterialStore = defineStore('material', () => {
  const material = ref<Material[]>([])
  const updateDialog = ref(false)
  const historyDialog = ref(false)
  const showDetails = ref(false)
  const importDialog = ref(false)
  const checkDialog = ref(false)
  const receiptDialog = ref(false)
  const All = ref(true)
  const Almost = ref(false)
  const Out = ref(false)
  const quantityIm = ref(1)
  const initialMaterial: Material = {
    material_id: 0,
    material_name: '',
    material_qty: 0,
    material_status: '',
    material_min: 0,
    material_unit: '',
    material_price: 0,
    material_Im: 0,
    material_max: 0
  }
  const incOrdec = ref(false)
  const historyReceiptStock = ref<ReceiptStock[]>([])

  const receiptStock = ref<ReceiptStock>({
    id: 1,
    createdDate: new Date(),
    createdDateString: '',
    total: 0,
    change: 0,
    paymentType: 'Cash',
    receiptStockItem: []
  })
  const StockItem = ref<StockItem[]>([])
  const editedMaterial = ref<Material>(JSON.parse(JSON.stringify(initialMaterial)))
  async function getMaterial(id: number) {
    const res = await materialService.getMaterial(id)
    editedMaterial.value = res.data
  }
  async function getMaterials() {
    const res = await materialService.getMaterials()
    material.value = res.data
  }
  async function saveMaterial() {
    const material = editedMaterial.value
    if (!material.material_id) {
      // Add new
      console.log('Post ' + JSON.stringify(material))
      const res = await materialService.addMaterial(material)
    } else {
      // Update
      console.log('Patch ' + JSON.stringify(material))
      const res = await materialService.updateMaterial(material)
    }
    await getMaterials()
  }

  async function deleteMaterial() {
    const material = editedMaterial.value
    const res = await materialService.delMaterial(material)
    await getMaterials()
  }
  function clearForm() {
    editedMaterial.value = JSON.parse(JSON.stringify(initialMaterial))
  }

  let lastId = 1
  function addReceiptStock(material: Material) {
    const index = StockItem.value.findIndex((item) => item.material?.material_id === material.material_id)
    if (index >= 0) {
      cal()
      if (incOrdec.value === true) {
        if (StockItem.value[index].unit === 1) {
          StockItem.value.splice(index, 1)
        } else {
          StockItem.value[index].unit--
        }
        incOrdec.value = false
      } else {
        StockItem.value[index].unit++
      }
      cal()
      return
    } else {
      const newReceipt: StockItem = {
        id: lastId,
        name: material.material_name,
        price: material.material_price * material.material_qty,
        unit: material.material_qty,
        stockId: material.material_id,
        material: material
      }
      StockItem.value.push(newReceipt)
      cal()
      lastId++
    }
  }

  function cal() {
    let total = 0
    for (const item of StockItem.value) {
      total = total + item.price * item.unit
    }
    receiptStock.value.total = total
  }

  function countAlmostStock() {
    let almostOutStock = 0
    material.value.forEach((item, index) => {
      if (
        material.value[index].material_min > material.value[index].material_qty &&
        material.value[index].material_qty != 0
      ) {
        almostOutStock++
      }
    })
    return almostOutStock
  }

  function countOutOfStock() {
    let outOfStock = 0
    material.value.forEach((item, index) => {
      if (material.value[index].material_qty === 0) {
        outOfStock++
      }
    })
    return outOfStock
  }

  function changeTime() {
    if (receiptStock.value.createdDate != undefined)
      receiptStock.value.createdDateString = receiptStock.value.createdDate.toLocaleString(
        'en-US',
        {
          day: 'numeric',
          month: 'numeric',
          year: 'numeric',
          hour: 'numeric',
          minute: 'numeric',
          hour12: false
        }
      )
  }
  function showImportDialog() {
    importDialog.value = true
  }
  function showCheckStock() {
    checkDialog.value = true
  }

  function showDetail(id: number) {
    const selectedReceiptHistory = historyReceiptStock.value.find((receipt) => receipt.id === id)

    if (selectedReceiptHistory) {
      const selected: ReceiptStock = {
        id: selectedReceiptHistory.id,
        createdDateString: selectedReceiptHistory.createdDateString,
        total: selectedReceiptHistory.total,
        change: selectedReceiptHistory.change,
        paymentType: selectedReceiptHistory.paymentType,
        receiptStockItem: selectedReceiptHistory.receiptStockItem
      }
      receiptStock.value = selected
      showDetails.value = true
    }
  }

  function inc(item: Material) {
    if(!item.material_Im) return
    item.material_Im++
    addReceiptStock(item)
  }
  function dec(item: Material) {
    if(!item.material_Im) return
    if (item.material_Im === 0) return
    incOrdec.value = true
    item.material_Im--
    addReceiptStock(item)
  }

  function addHistory() {
    const receipts = { ...receiptStock.value }

    if (historyReceiptStock.value.length === 0) {
      lastId = 1
    } else {
      lastId = historyReceiptStock.value.length
      lastId++
    }
    receipts.id = lastId
    historyReceiptStock.value.push(receipts)
  }

  function clear() {
    StockItem.value = []
    receiptStock.value = {
      id: 0,
      createdDate: new Date(),
      createdDateString: '',
      total: 0,
      change: 0,
      paymentType: 'Cash',
      receiptStockItem: []
    }
    lastId = 1
  }

  const tableStock = ref<Material[]>([])
  function changeTable() {
    if (Almost.value === true) {
      All.value = false
      Out.value = false
      tableStock.value = material.value.filter(
        (item) => item.material_qty < item.material_min && item.material_qty > 0
      )
      nextTick(() => {
        Almost.value = false
      })
    } else if (Out.value === true) {
      Almost.value = false
      All.value = false
      tableStock.value = material.value.filter((item) => item.material_qty <= 0)
      nextTick(() => {
        Out.value = false
      })
    } else if (All.value === true) {
      getMaterials()
      Out.value = false
      Almost.value = false
      tableStock.value = material.value
      nextTick(() => {
        All.value = false
      })
    }
  }
  return { material, getMaterial, saveMaterial, deleteMaterial, getMaterials, clearForm, editedMaterial, initialMaterial, addReceiptStock, countOutOfStock, countAlmostStock, cal, importDialog,
    quantityIm, checkDialog, receiptStock, StockItem, historyDialog, updateDialog, historyReceiptStock, showDetails, receiptDialog, All, Almost, Out, tableStock, showImportDialog,
    dec, inc, showCheckStock, clear, changeTime, addHistory, showDetail, changeTable}
})
