import { ref } from 'vue'
import { defineStore } from 'pinia'
import type { Member } from '@/types/Member'
import memberService from '@/service/member'

export const useMemberStore = defineStore('member', () => {
  const memberDialog = ref(false)
  const members = ref<Member[]>([])
  const currentMember = ref<Member | null>()
  const initilUser: Member = {
    tel: '',
    fullName: '',
    email: '',
    password: '',
    birthday: '',
    point: '',
    usedPoint: ''
  }
  const editedMember = ref<Member>(JSON.parse(JSON.stringify(initilUser)))

  const searchMember = (tel: string) => {
    const index = members.value.findIndex((item) => item.tel === tel)
    if (index < 0) {
      currentMember.value = null
    }
    currentMember.value = members.value[index]
  }

  function clear() {
    currentMember.value = null
  }

  const addMember = (newMember: Member) => {
    members.value.push(newMember)
    memberDialog.value = false
  }

  async function getMember(id: number) {
    const res = await memberService.getMember(id)
    editedMember.value = res.data
  }

  async function getMembers() {
    const res = await memberService.getMembers()
    members.value = res.data
  }

  async function saveMembers() {
    const member = editedMember.value
    if (!member.id) {
      const res = await memberService.addMember(member)
    } else {
      const res = await memberService.updateMember(member)
    }
    await getMembers()
  }

  async function deleteMembers() {
    const member = editedMember.value
    const res = await memberService.delMember(member)
    await getMembers()
  }

  function clearForm() {
    editedMember.value = JSON.parse(JSON.stringify(initilUser))
  }

  function getCurrentMember(): Member | null {
    const strUser = localStorage.getItem('member')
    if (strUser === null) return null
    return JSON.parse(strUser)
  }
  return {
    addMember,
    memberDialog,
    members,
    searchMember,
    currentMember,
    clear,
    getMember,
    getMembers,
    saveMembers,
    deleteMembers,
    editedMember,
    clearForm,
    getCurrentMember
  }
})
