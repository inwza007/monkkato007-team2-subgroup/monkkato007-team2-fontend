import { ref } from 'vue'
import { defineStore } from 'pinia'
import type { Bill } from '@/types/Bill'
import { useMessageStore } from './message'
import billService from '@/service/bill'

export const useBillStore = defineStore('bill', () => {
  const messageStore = useMessageStore()
  const bills = ref<Bill[]>([])
  const initialBill: Bill & { files: File[] } = {
    branchId: '',
    userId: '',
    detail: '',
    amount: 0,
    image: 'noimage.jpg',
    files: []
  }
  const editedBill = ref<Bill & { files: File[] }>(JSON.parse(JSON.stringify(initialBill)))

  async function getBill(id: number) {
    try {
      const res = await billService.getBill(id)
      editedBill.value = res.data
    } catch (e: any) {
      messageStore.showMessage(e.message)
    }
  }
  async function getBills() {
    try {
      const res = await billService.getBills()
      bills.value = res.data
    } catch (e: any) {
      messageStore.showMessage(e.message)
    }
  }
  async function saveBill() {
    try {
      const bill = editedBill.value
      if (!bill.id) {
        // Add new
        console.log('Post ' + JSON.stringify(bill))
        const res = await billService.addBill(bill)
      } else {
        // Update
        console.log('Patch ' + JSON.stringify(bill))
        const res = await billService.updateBill(bill)
      }
      await getBills()
    } catch (e: any) {
      messageStore.showMessage(e.message)
    }
  }
  async function deleteBill() {
    const bill = editedBill.value
    const res = await billService.delBill(bill)

    await getBills()
  }

  function clearForm() {
    editedBill.value = JSON.parse(JSON.stringify(initialBill))
  }
  return { bills, getBills, saveBill, deleteBill, editedBill, getBill, clearForm }
})
