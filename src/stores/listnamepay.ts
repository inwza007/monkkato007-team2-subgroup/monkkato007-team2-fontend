import { computed, ref } from 'vue'
import type { User } from '@/types/User'
import { defineStore } from 'pinia'
import type { Listnamepay } from '@/types/Listnamepay'

export const uselistnamepayStore = defineStore('listnamepay', () => {
  const AddDialog = ref(false)
  const check = ref(false)
  const listnamepay = ref<Listnamepay[]>([
    // {
    //   id: 1,
    
      
    //   name: 'Chanon Thansawad',

    //   roles: 'user',
    //   workhours: 8,
     
    //  salary: 0,
    //  status: '',
    //  date: new Date()
    // }
  ])

  function showAddDialog() {
    AddDialog.value = true
  }

  const deleteEmployee = (employeeId: number) => {
    console.log('ลบพนักงานที่มี ID:', employeeId)

    const index = listnamepay.value.findIndex((employee) => employee.id == employeeId)

    if (index !== -1) {
      listnamepay.value.splice(index, 1)
      console.log('ลบพนักงานเรียบร้อยแล้ว.')
    } else {
      console.error('ไม่พบพนักงานใน Id', employeeId)
    }
  }

  const Sumsalary = computed(() => {
    const sum = listnamepay.value.reduce((total, employee) => total + employee.salary, 0)
    return parseFloat(sum.toFixed(3))
  })

  function removeall() {
    listnamepay.value = []
    console.log('ลบทั้งหมดเรียบร้อยแล้ว.')
  }

  return {
    AddDialog,
    listnamepay,
    Sumsalary,
    showAddDialog,
    deleteEmployee,
    removeall
  }
})
