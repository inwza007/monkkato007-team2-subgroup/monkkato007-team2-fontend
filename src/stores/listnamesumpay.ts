import { ref } from 'vue'
import type { ListnameSumpay } from '@/types/ListnameSumpay'
import type { Listnamepay } from '@/types/Listnamepay'
import { uselistnamepayStore } from './listnamepay'

import ListNameAddVue from '@/views/Payment/ListNameAdd.vue'
import ListNameSumAdd from '@/views/Payment/ListNameSumAdd.vue'

import { defineStore } from 'pinia'

export const uselistnamesumpayStore = defineStore('listnamesumpay', () => {
  const pays = ref(ListNameSumAdd)
  const listNamepay = uselistnamepayStore()
  let Round = 1
  const AddDialog = ref(false)
  const listnamepay = ref<Listnamepay[]>([])
  const listnamesumpay = ref<ListnameSumpay[]>([])

  function showAddDialog() {
    AddDialog.value = true
  }

  function sumsalary(pay: any) {
    const currentTime = ref(new Date())
    // const sumSalary = listNamepay.Sumsalary.toFixed(3);
    const sumSalary = parseFloat(listNamepay.Sumsalary.toFixed(3))

    const day = currentTime.value.getDate()
    const month = currentTime.value.getMonth() + 1
    const year = currentTime.value.getFullYear()
    if (pay == sumSalary) {
      const num = listNamepay.listnamepay.length

      const roundData: ListnameSumpay = {
        Round: Round,
        date: `${day}/${month}/${year}`,
        //  day : currentTime.value.getDate(),
        //  month : currentTime.value.getMonth() + 1,
        //   year : currentTime.value.getFullYear(),
        time: currentTime.value.toLocaleTimeString(),
        SumSalary: parseFloat(sumSalary.toFixed(3)),
        num: num,
        Status: 'อนุมัติแล้ว',
        name: listNamepay.listnamepay.map((item) => item.name),
        Salary: listNamepay.listnamepay.map((item) => parseFloat(item.salary.toFixed(3)))
      }

      listnamesumpay.value.push(roundData)

      Round++
      //  ลบข้อมูลทั้งหมด
      // listNamepay.removeall()
    } else {
      console.log('เงินไม่ตรง')
      console.log(pay)
    }
    
  }

  return {
    AddDialog,
    listnamesumpay,
    showAddDialog,
    sumsalary
  }
})
