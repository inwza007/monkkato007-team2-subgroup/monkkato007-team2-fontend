import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useDateStore = defineStore('Date', () => {
  const currentDate = ref(new Date())

  const day = computed(() => currentDate.value.getDate())
  const month = computed(() => currentDate.value.getMonth() + 1)
  const year = computed(() => currentDate.value.getFullYear())
  const hour = computed(() => currentDate.value.getHours())
  const minute = computed(() => currentDate.value.getMinutes())

  const monthEn = computed(() => currentDate.value.toLocaleString('en-US', { month: 'long' }))

  const formattedHour = computed(() => String(hour.value).padStart(2, '0'))
  const formattedMinute = computed(() => String(minute.value).padStart(2, '0'))

  function changeTime(date: Date) {
    return date.toLocaleString('en-US', {
      day: 'numeric',
      month: 'numeric',
      year: 'numeric',
      hour: 'numeric',
      minute: 'numeric',
      hour12: false
    })
  }
  function getFormattedDateTime() {
    return {
      day: day.value,
      month: monthEn.value,
      year: year.value,
      formattedHour: formattedHour.value,
      formattedMinute: formattedMinute.value
    }
  }
  function clear() {
    currentDate.value = new Date()
  }

  return {
    currentDate,
    day,
    month,
    year,
    hour,
    minute,
    monthEn,
    formattedHour,
    formattedMinute,
    getFormattedDateTime,
    clear,
    changeTime
  }
})
