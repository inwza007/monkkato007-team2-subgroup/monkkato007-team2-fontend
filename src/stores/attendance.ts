import { ref } from 'vue'
import { defineStore } from 'pinia'
import type { Attendance } from '@/types/Attendance'
import attendanceService from '@/service/attendance'

export const useAttendanceStore = defineStore('attendance', () => {
  const attendanceDialog = ref(false)
  const attendances = ref<Attendance[]>([])
  const currentAttendance = ref<Attendance | null>()

  const initilAttendance: Attendance = {
    email: '',
    fullName: '',
    timeIn: null,
    timeOut: null
  }
  const editedAttendance = ref<Attendance>(JSON.parse(JSON.stringify(initilAttendance)))

  function clear() {
    currentAttendance.value = null
  }

  const addAttendance = (attendance: Attendance) => {
    attendances.value.push(attendance)
    attendanceDialog.value = false
  }

  async function getAttendance(id: number) {
    const res = await attendanceService.getAttendance(id)
    editedAttendance.value = res.data
  }

  async function getAttendances() {
    const res = await attendanceService.getAttendances()
    attendances.value = res.data
    console.log(attendances)
  }

  async function saveAttendance() {
    const attendances = editedAttendance.value
    if (!attendances.id) {
      const res = await attendanceService.addAttendance(attendances)
    } else {
      const res = await attendanceService.updateAttendance(attendances)
    }
    await getAttendances()
  }

  async function deleteAttendance() {
    const attendance = editedAttendance.value
    const res = await attendanceService.delAttendance(attendances)
    await getAttendances()
  }

  function clearForm() {
    editedAttendance.value = JSON.parse(JSON.stringify(initilAttendance))
  }
  return {
    addAttendance,
    attendanceDialog,
    attendances,
    currentAttendance,
    clear,
    getAttendance,
    getAttendances,
    saveAttendance,
    deleteAttendance,
    editedAttendance,
    clearForm
  }
})
