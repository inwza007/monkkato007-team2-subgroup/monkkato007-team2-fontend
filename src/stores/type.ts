import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import typeService from '@/service/type'
import type { Type } from '@/types/Type'
import { useMessageStore } from './message'

export const useTypeStore = defineStore('type', () => {
  const messageStore = useMessageStore()
  const type = ref<Type[]>([])
  const initialType: Type = {
    name: ''
  }
  const editedType = ref<Type>(JSON.parse(JSON.stringify(initialType)))

  async function getType(id: number) {
    const res = await typeService.getType(id)
    editedType.value = res.data
  }
  async function getTypes() {
    try {
      const res = await typeService.getTypes()
      // console.log(res.data)
      type.value = res.data
    } catch (e: any) {
      messageStore.showMessage(e.message)
    }
  }
  async function saveType() {
    const type = editedType.value
    if (!type.id) {
      // Add new
      console.log('Post ' + JSON.stringify(type))
      const res = await typeService.addType(type)
    } else {
      // Update
      console.log('Patch ' + JSON.stringify(type))
      const res = await typeService.updateType(type)
    }

    await getTypes()
  }
  async function deleteType() {
    const type = editedType.value
    const res = await typeService.delType(type)
    await getTypes()
  }

  function clearForm() {
    editedType.value = JSON.parse(JSON.stringify(initialType))
  }
  return { type, getTypes, saveType, deleteType, editedType, getType, clearForm }
})
