import type { Stock } from "@/types/stock";
import { defineStore } from "pinia";
import { ref } from "vue";
import stockService from '@/service/stock';
import materialService from '@/service/material';
import type { Material } from "@/types/Material";

export const useStockStore = defineStore('stock', () => {
    const stock = ref<Stock[]>([])
    const material = ref<Material[]>([])
    const initialStock: Stock ={
        stock_id: 0,
        stock_qty: 0,
        material_status: "",
        materialId: 0,
        branchId: 0,
        material: {
            material_id: 0,
            material_name: "",
            material_qty: 0,
            material_status: "",
            material_min: 0,
            material_max: 0,
            material_price: 0,
            material_unit: "",
            material_Im: 0
        },
        branch: {
            id: undefined,
            name: "",
            tel: "",
            addres: ""
        }
    }
    const editedStock = ref<Stock>(JSON.parse(JSON.stringify(initialStock)))
    
    async function getStock(id: number){
        const res = await stockService.getStock(id)
        editedStock.value = res.data
    }

    async function getStocks(){
        const res = await stockService.getStocks()
        stock.value = res.data
    }

    async function saveStock(){
        const stock = editedStock.value
        if(!stock.stock_id){
            const res = await stockService.addStock(stock)
        }else{
            const res = await stockService.updateStock(stock)
        }
        await getStocks()
    }

    async function deleteStock(){
        const stock = editedStock.value
        const res = await stockService.delStock(stock)
        await getStocks()
    }

    function clearForm(){
        editedStock.value = JSON.parse(JSON.stringify(initialStock))
    }

    function changeTable(){

    }
    //let lastId = 1
    
    return { stock, initialStock, editedStock, getStock, getStocks, saveStock, deleteStock, clearForm, material }
  })