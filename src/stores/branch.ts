import { ref } from 'vue'
import { defineStore } from 'pinia'
import type { Branch } from '@/types/Branch'
import { useMessageStore } from './message'
import branchService from '@/service/branch'

export const useBranchStore = defineStore('branch', () => {
  const messageStore = useMessageStore()
  const branchs = ref<Branch[]>([])
  const initialBranch: Branch = {
    name: '',
    tel: '',
    addres: ''
  }
  const editedBranch = ref<Branch>(JSON.parse(JSON.stringify(initialBranch)))

  function getCurrentBranch(): Branch | null {
    const strUser = localStorage.getItem('branch')
    if (strUser === null) return null
    console.log('test ' + JSON.parse(strUser))
    return JSON.parse(strUser)
  }

  async function getBranch(id: number) {
    try {
      const res = await branchService.getBranch(id)
      editedBranch.value = res.data
    } catch (e: any) {
      messageStore.showMessage(e.message)
    }
  }
  async function getBranches() {
    try {
      const res = await branchService.getBranches()
      branchs.value = res.data
    } catch (e: any) {
      messageStore.showMessage(e.message)
    }
  }
  async function saveBranch() {
    try {
      const branch = editedBranch.value
      if (!branch.id) {
        // Add new
        console.log('Post ' + JSON.stringify(branch))
        const res = await branchService.addBranch(branch)
      } else {
        // Update
        console.log('Patch ' + JSON.stringify(branch))
        const res = await branchService.updateBranch(branch)
      }
      await getBranches()
    } catch (e: any) {
      messageStore.showMessage(e.message)
    }
  }
  async function deleteBranch() {
    const branch = editedBranch.value
    const res = await branchService.delBranch(branch)

    await getBranches()
  }

  function clearForm() {
    editedBranch.value = JSON.parse(JSON.stringify(initialBranch))
  }
  return {
    branchs,
    getBranch,
    saveBranch,
    deleteBranch,
    editedBranch,
    clearForm,
    getBranches,
    getCurrentBranch
  }
})
