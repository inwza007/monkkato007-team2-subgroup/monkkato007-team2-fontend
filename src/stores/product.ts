import { ref } from 'vue'
import { defineStore } from 'pinia'
import type { Product } from '@/types/Product'
import { useMessageStore } from './message'
import productService from '@/service/product'

export const useProductStore = defineStore('product', () => {
  const messageStore = useMessageStore()
  const products = ref<Product[]>([])
  const initialProduct: Product & { files: File[] } = {
    name: '',
    price: 0,
    type: { id: 2, name: 'dessert' },
    subType: ['Cool'],
    size: ['S'],
    sweetLevel: ['50%'],
    image: 'noimage.jpg',
    files: []
  }
  const editedProduct = ref<Product & { files: File[] }>(JSON.parse(JSON.stringify(initialProduct)))

  async function getProduct(id: number) {
    try {
      const res = await productService.getProduct(id)
      editedProduct.value = res.data
    } catch (e: any) {
      messageStore.showMessage(e.message)
    }
  }
  async function getProducts() {
    try {
      const res = await productService.getProducts()
      products.value = res.data
    } catch (e: any) {
      messageStore.showMessage(e.message)
    }
  }
  async function saveProduct() {
    try {
      const product = editedProduct.value
      if (!product.id) {
        // Add new
        console.log('Post ' + JSON.stringify(product))
        const res = await productService.addProduct(product)
      } else {
        // Update
        console.log('Patch ' + JSON.stringify(product))
        const res = await productService.updateProduct(product)
      }
      await getProducts()
    } catch (e: any) {
      messageStore.showMessage(e.message)
    }
  }
  async function deleteProduct() {
    const product = editedProduct.value
    const res = await productService.delProduct(product)

    await getProducts()
  }

  function clearForm() {
    editedProduct.value = JSON.parse(JSON.stringify(initialProduct))
  }
  return { products, getProducts, saveProduct, deleteProduct, editedProduct, getProduct, clearForm }
})
