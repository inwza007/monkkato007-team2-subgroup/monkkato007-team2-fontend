import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { useLoadingStore } from './loading'
import promotionService from '@/service/promotion'
import type { Promotion } from '@/types/Promotion'

export const usePromotionStore = defineStore('promotion', () => {
  const loadingStone = useLoadingStore()
  const promotions = ref<Promotion[]>([])
  async function getPromotion(id: number) {
    loadingStone.doLoad()
    const res = await promotionService.getPromotion(id)
    promotions.value = res.data
    loadingStone.finish()
  }
  async function getPromotions() {
    loadingStone.doLoad()
    const res = await promotionService.getPromotions()
    promotions.value = res.data
    loadingStone.finish()
  }

  async function savePromotion(promotion: Promotion) {
    loadingStone.doLoad()
    if (promotion.id < 0) {
      console.log('Post' + JSON.stringify(promotions))
      //add new
      const res = await promotionService.addPromotion(promotion)
    } else {
      console.log('Patch' + JSON.stringify(promotions))
      //update
      const res = await promotionService.updatePromotion(promotion)
    }
    await getPromotions()
    loadingStone.finish()
  }

  async function deletePromotion(promotion: Promotion) {
    loadingStone.doLoad()
    const res = await promotionService.delPromotion(promotion)
    await getPromotions()
    loadingStone.finish()
  }

  return { promotions, getPromotions, savePromotion, deletePromotion, getPromotion }
})
