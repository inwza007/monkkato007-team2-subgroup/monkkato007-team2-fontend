import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Product } from '@/types/Product'
import productService from '@/service/product'
import { useMessageStore } from './message'

export const usePosStore = defineStore('pos', () => {
  const messageStore = useMessageStore()
  const products1 = ref<Product[]>([])
  const products2 = ref<Product[]>([])
  const products3 = ref<Product[]>([])
  async function getProducts() {
    try {
      let res = await productService.getProductsByType(1)
      products1.value = res.data
      res = await productService.getProductsByType(2)
      products2.value = res.data
      res = await productService.getProductsByType(3)
      products3.value = res.data
    } catch (e: any) {
      messageStore.showMessage(e.message)
    }
  }
  return { products1, products2, products3, getProducts }
})
