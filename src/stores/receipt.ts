import { ref, watch } from 'vue'
import { defineStore } from 'pinia'
import type { ReceiptItem } from '@/types/ReceiptItem'
import type { Receipt } from '@/types/Receipt'
import ReceiptService from '@/service/receipt'
import { useAuthStore } from './auth'
import { useMemberStore } from './member'
import { useMessageStore } from './message'
import type { Member } from '@/types/Member'
import { useBranchStore } from './branch'

export const useReceiptStore = defineStore('receipt', () => {
  const memberStore = useMemberStore()
  const branchStore = useBranchStore()
  const authStore = useAuthStore()
  const messageStore = useMessageStore()
  const receiptDialog = ref(false)
  const receiptDialogDetail = ref(false)
  const receiptItems = ref<ReceiptItem[]>([])
  const receipts = ref<Receipt[]>([])
  const receipt = ref<Receipt>()
  initReceipt()
  function initReceipt() {
    receipt.value = {
      createdDate: new Date(),
      total: 0,
      discount: 0,
      net: 0,
      amount: 0,
      change: 0,
      paymentType: '',
      userId: authStore.getCurrentUser()!.id!,
      memberId: 0,
      members: memberStore.getCurrentMember()!,
      user: authStore.getCurrentUser()!,
      branchId: 1,
      branchs: branchStore.getCurrentBranch()!
    }
    receiptItems.value = []
  }

  watch(
    receiptItems,
    () => {
      calReceipt()
    },
    {
      deep: true
    }
  )
  const calReceipt = function () {
    console.log('Cal')
    // console.log(receipt.value.member)
    receipt.value!.total = 0
    // receipt.value!.amount = 0
    console.log(receipt.value!.net)
    for (let i = 0; i < receiptItems.value.length; i++) {
      receipt.value!.total += receiptItems.value[i].price * receiptItems.value[i].qty
      // receipt.value!.amount += receiptItems.value[i].qty
    }
    receipt.value!.net = receipt.value!.total - receipt.value!.discount
  }
  const addReceiptItem = (newReceiptItem: ReceiptItem) => {
    const existingItemIndex = receiptItems.value.findIndex(
      (item) => item.productId === newReceiptItem.productId
    )
    if (existingItemIndex !== -1) {
      // หากมีสินค้าที่มี productId เดียวกันอยู่แล้วในรายการ
      receiptItems.value[existingItemIndex].qty += newReceiptItem.qty
    } else {
      // หากไม่มีสินค้าที่ซ้ำกันในรายการให้เพิ่มสินค้าใหม่
      receiptItems.value.push(newReceiptItem)
    }
  }
  const deleteReceiptItem = (selectedItem: ReceiptItem) => {
    const index = receiptItems.value.findIndex((item) => item === selectedItem)
    receiptItems.value.splice(index, 1)
  }
  const incqtyOfReceiptItem = (selectedItem: ReceiptItem) => {
    selectedItem.qty++
  }
  const decqtyOfReceiptItem = (selectedItem: ReceiptItem) => {
    selectedItem.qty--
    if (selectedItem.qty === 0) {
      deleteReceiptItem(selectedItem)
    }
    calReceipt()
  }
  const removeItem = (item: ReceiptItem) => {
    const index = receiptItems.value.findIndex((ri) => ri === item)
    receiptItems.value.splice(index, 1)
    calReceipt()
  }

  const updateCurrentMemberInReceipt = (member: Member | null) => {
    if (member) {
      const memberId = member.id
      if (receipt.value) {
        receipt.value.members = member
        receipt.value.memberId = member.id || 0
        console.log('after add member ' + JSON.stringify(receipt.value.members))
        console.log('after add member ' + JSON.stringify(receipt.value.memberId))
        console.log('store receipt branch ' + JSON.stringify(branchStore.getCurrentBranch()))
        console.log('store receipt branch ' + JSON.stringify(receipt.value.branchId))
      }
      console.log(JSON.stringify(branchStore.getBranch(1)))
    }
  }

  const payment = ref('Select Payment')
  function updatePaymentType(selectedPayment: string) {
    console.log('Selected Payment:', selectedPayment)
    const Amount = 0
    if (receipt.value) {
      receipt.value.paymentType = selectedPayment
      console.log('Updated Payment Type:', receipt.value.paymentType)
      if (receipt.value.paymentType === 'Cash') {
        console.log('Selected Cash')
        receipt.value.amount = Amount
        receipt.value.change = receipt.value.amount - receipt.value.total
      } else if (receipt.value.paymentType === 'PromptPay') {
        console.log('Selected PromptPay')
        receipt.value.amount = receipt.value.total
      } else if (receipt.value.paymentType === 'Credit') {
        console.log('Selected Credit')
        receipt.value.amount = receipt.value.total
      }
      payment.value = selectedPayment
      receipt.value.change = receipt.value.amount - receipt.value.total
    }
  }

  function calReceivedAmount(Amount: number) {
    receipt.value!.amount = Amount
    receipt.value!.change = receipt.value!.amount - receipt.value!.total
  }

  function checkPayment() {
    if (payment.value === 'PromptPay') {
      receiptDialog.value = true
    } else if (payment.value === 'Cash' && receipt.value!.change >= 0) {
      receiptDialog.value = true
    } else if (payment.value === 'Select Payment') {
      receiptDialog.value = false
      messageStore.showMessage('Please select payType before payment.')
    }
  }

  async function getReceipt(id: number) {
    try {
      const res = await ReceiptService.getReceitp(id)
      receipts.value = res.data
    } catch (e: any) {
      messageStore.showMessage(e.message)
    }
  }
  async function getReceipts() {
    try {
      const res = await ReceiptService.getReceitps()
      receipts.value = res.data
    } catch (e: any) {
      messageStore.showMessage(e.message)
    }
  }
  async function loadReceiptDetail(receiptId: number) {
    console.log('store ' + receiptId)

    try {
      // ค้นหาใบเสร็จที่มี receiptId ตรงกัน
      receipt.value = receipts.value.find((receipt) => receipt.id === receiptId)
      if (receipt.value) {
        // ส่งใบเสร็จที่เลือกไปยังหน้าต่างแสดงรายละเอียด
        console.log('receipt ' + JSON.stringify(receipt.value))
        console.log('member ' + JSON.stringify(receipt.value.members))
        receiptDialogDetail.value = true
      } else {
        console.error(`Receipt with id ${receiptId} not found`)
      }
    } catch (error) {
      console.error('Error loading receipt detail:', error)
    }
  }

  async function addreceipts() {
    try {
      await ReceiptService.addOrder(receipt.value!, receiptItems.value)
      console.log('stoer memberID' + JSON.stringify(memberStore.currentMember))
      // receiptDialog.value = true
      initReceipt()
    } catch (e: any) {
      console.log(receiptItems.value)
      messageStore.showMessage(e.message)
    }
  }

  function clear() {
    initReceipt()
  }

  const currentStep = ref(1)

  const nextStep = () => {
    if (receiptItems.value.length > 0) {
      currentStep.value++
    } else {
      alert('Please select at least one product before proceeding to the next step.')
    }
  }

  const prevStep = () => {
    if (currentStep.value > 1) {
      currentStep.value--
    }
  }
  return {
    updatePaymentType,
    receipt,
    receiptItems,
    addReceiptItem,
    incqtyOfReceiptItem,
    decqtyOfReceiptItem,
    deleteReceiptItem,
    removeItem,
    currentStep,
    nextStep,
    prevStep,
    clear,
    updateCurrentMemberInReceipt,
    calReceipt,
    receipts,
    initReceipt,
    calReceivedAmount,
    receiptDialogDetail,
    receiptDialog,
    getReceipt,
    getReceipts,
    loadReceiptDetail,
    addreceipts,
    payment,
    checkPayment
  }
})
