import { ref } from 'vue'
import { defineStore } from 'pinia'
import type { User } from '@/types/User'
import authService from '@/service/auth'
import { useMessageStore } from '@/stores/message'
import router from '@/router'

export const useAuthStore = defineStore('auth', () => {
  const login = async function (email: string, password: string) {
    const messageStore = useMessageStore()
    logout()
    try {
      const res = await authService.login(email, password)
      console.log(res.data)
      localStorage.setItem('user', JSON.stringify(res.data.user))
      localStorage.setItem('access_token', res.data.access_token)
      messageStore.showMessage('Login Success')
      router.push('/home')
    } catch (e: any) {
      messageStore.showMessage(e.message)
    }
  }
  function getCurrentUser(): User | null {
    const strUser = localStorage.getItem('user')
    if (strUser === null) return null
    return JSON.parse(strUser)
  }
  const logout = function () {
    localStorage.removeItem('user')
    localStorage.removeItem('access_token')
    router.push('/login')
  }
  function getToken(): string | null {
    const strToken = localStorage.getItem('access_token')
    if (strToken === null) return null
    return JSON.parse(strToken)
  }
  return { getCurrentUser, login, getToken, logout }
})
